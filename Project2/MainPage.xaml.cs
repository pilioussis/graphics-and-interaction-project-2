﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using System;
namespace Project2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        private Project2Game game;

        public MainPage()
        {
            InitializeComponent();
            game = new Project2Game(this);
            game.Run(this);
        }

        private void StartGame(object sender, RoutedEventArgs s)
        {
            if (game.keyboardsOnly)
                jumpBtn.Visibility = Visibility.Collapsed;
            game.started = true;
            balls.Visibility = Visibility.Collapsed;
            Title.Visibility = Visibility.Collapsed;
            hoff.Visibility = Visibility.Collapsed;
            cmdStart.Visibility = Visibility.Collapsed;
            randomString.Visibility = Visibility.Visible;
            cashTxt.Visibility = Visibility.Visible;
            resetScore.Visibility = Visibility.Collapsed;
        }

     

        private void RestartGame(object sender, RoutedEventArgs s)
        {
            game.started = true;
            game.timeLeft = Project2Game.TOTAL_TIME;
            ((Player)game.gameObjects[1]).pos = new SharpDX.Vector3(City.WIDTH / 2, City.heightMax * 1.2f, City.WIDTH / 2);
            Player.speed = 0;
            ((Player)game.gameObjects[1]).resetCash();

            hoff.Visibility = Visibility.Collapsed;
            finalScoreScreen.Visibility = Visibility.Collapsed;
            retryButton.Visibility = Visibility.Collapsed;
            randomString.Visibility = Visibility.Visible;
            resetScore.Visibility = Visibility.Collapsed;
            game.wroteAlready = false;

        }

        public void EndGame()
        {
            resetScore.Visibility = Visibility.Visible;
            game.started = false;
            hoff.Visibility = Visibility.Visible;
            finalScoreScreen.Visibility = Visibility.Visible;
            retryButton.Visibility = Visibility.Visible;
            randomString.Visibility = Visibility.Collapsed;
        }

        public void ShowCash(String s)
        {
            cashTxt.Text = "$"+s;
            finalScoreScreen.Text = "Congratulations!! You're worth:\n\t  $" + s +"\n\tHoff is alright!";
        }

        public void ShowHighScore(String s)
        {
            highScore.Text = "High Score: $" + s;
        }

        public void UpdateHighScore(String s)
        {
            highScore.Visibility = Visibility.Collapsed;
            highScore.Text = "$" + s;
            highScore.Visibility = Visibility.Visible;
        }

        public void showRandomString(String s)
        {
             randomString.Text = s;
        }

        private void manstarting(object sender, Windows.UI.Xaml.Input.ManipulationStartingRoutedEventArgs e)
        {
            ((Player)game.gameObjects[1]).charging = true;
            System.Diagnostics.Debug.WriteLine("starting");
          
        }



        private void click(object sender, RoutedEventArgs e)
        {
            ((Player)game.gameObjects[1]).charging = false;
            System.Diagnostics.Debug.WriteLine("click");
        }

        private void ended(object sender, Windows.UI.Xaml.Input.ManipulationCompletedRoutedEventArgs e)
        {

        }

        private void delta(object sender, Windows.UI.Xaml.Input.ManipulationDeltaRoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("delta " + e.Delta.Translation.Y);
            ((Player)game.gameObjects[1]).chargeAmount = (float) e.Delta.Translation.Y/200;
            
        }

        private void resetScore_Click(object sender, RoutedEventArgs e)
        {
                game.ResetScore();
        }

        private void jump(object sender, RoutedEventArgs e)
        {
            game.player.jumpPower = game.player.MAXJUMPPOWER;
        }

        private void menu(object sender, RoutedEventArgs e)
        {
            game.started = true;
            balls.Visibility = Visibility.Visible;
            Title.Visibility = Visibility.Visible;
            hoff.Visibility = Visibility.Visible;
            cmdStart.Visibility = Visibility.Visible;
            randomString.Visibility = Visibility.Collapsed;
            cashTxt.Visibility = Visibility.Collapsed;
            resetScore.Visibility = Visibility.Visible;
        }





    }
}
