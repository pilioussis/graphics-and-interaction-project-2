﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;
namespace Project2
{
    public class Camera
    {
        public Project2Game game;

        //camera output Matrices
        public Matrix View;
        public Matrix Projection;

        //camera control variables
        public Vector3 cameraPos;
        public Vector3 cameraTarget;
        public float viewAngle;
        public float rotationY = 1f; //Turn counter clockwise when increasing (range: 0,2pi)
        public float rotationX = -0.3f; //look up when increasing (range: -pi/2, pi/2)

        // view angle effect controls.
        public static readonly float MAX_FOCAL_LENGTH = 2.0f;
        public static readonly float MIN_FOCAL_LENGTH = 1.3f;
        public static readonly float ZOOM_SPEED = 0.01f;
        public static readonly float FOCAL_LENGTH_BUFFER = 0.1f;
        public static readonly float STARTING_VIEW_ANGLE = 1.0f;

        private readonly float CAMERADISTANCE = 6.5f;

        // Ensures that all objects are being rendered from a consistent viewpoint
        public Camera(Project2Game game)
        {
            viewAngle = STARTING_VIEW_ANGLE;
            cameraPos = new Vector3(-4, 8, -24);
            View = Matrix.LookAtLH(new Vector3(0, 0, -10), new Vector3(0, 0, 0), Vector3.UnitY);
            Projection = Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.1f, 100.0f);
            this.game = game;
        }

        // If the screen is resized, the projection matrix will change
        public void Update()
        {
            adjustCamera();
            calculateViewAnlge();
            
            cameraTarget = new Vector3((float)(cameraPos.X + Math.Cos(rotationY) * Math.Cos(rotationX)),
                                       (float)(cameraPos.Y + Math.Sin(rotationX)),
                                       (float)(cameraPos.Z + Math.Sin(rotationY) * Math.Cos(rotationX)));

            //projection
            View = Matrix.LookAtLH(cameraPos,
                                    cameraTarget,
                                    Vector3.UnitY);
            Projection = Matrix.PerspectiveFovLH((float)Math.PI / viewAngle, (float)game.GraphicsDevice.BackBuffer.Width / game.GraphicsDevice.BackBuffer.Height, 0.001f, 10000.0f);

        }

        //this function controls how the viewing angle changes with respect to speed, lagging the viewing angle to
        //create smoother movement.
        public void calculateViewAnlge()
        {
            //use formula to calculate new viewangle with respect to player speed.
            float newViewAngle = MAX_FOCAL_LENGTH - (Player.speed / Player.SPEEDLIMIT * (MAX_FOCAL_LENGTH - MIN_FOCAL_LENGTH));
            if (newViewAngle < (MIN_FOCAL_LENGTH - Player.ACCELERATION))
                newViewAngle = MIN_FOCAL_LENGTH + FOCAL_LENGTH_BUFFER;

            //Only ever change viewing angle by a small amount.
            if (newViewAngle - viewAngle > FOCAL_LENGTH_BUFFER)
            {
                viewAngle += ZOOM_SPEED;
            }
            else if (newViewAngle - viewAngle < -FOCAL_LENGTH_BUFFER)
                viewAngle -= ZOOM_SPEED;
        }

        //make camera follow players position and direction
        private void adjustCamera()
        {
            cameraPos.X = game.player.pos.X - ((float)Math.Cos(game.player.direction) * CAMERADISTANCE);
            cameraPos.Z = game.player.pos.Z - ((float)Math.Sin(game.player.direction) * CAMERADISTANCE);
            cameraPos.Y = game.player.pos.Y + 4;
            rotationY = game.player.direction;
        }

    }
}
