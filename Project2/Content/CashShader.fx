﻿float4x4 World;
float4x4 View;
float4x4 Projection;
float4x4 WorldInverseTranspose;
texture2D ModelTexture;
 
float4 AmbientColor = float4(1, 1, 1, 1);
float AmbientIntensity = 0.1;
 
float3 DiffuseLightDirection = float3(10, 1, 4);
float4 DiffuseColor = float4(1, 1, 1, 1);
float DiffuseIntensity = 0.8;
 
//float Shininess = 200;
//float4 SpecularColor = float4(1, 1, 1, 1);
//float SpecularIntensity = 0;
//float4 ViewVector = float4(1, 0, 0, 0);
// ViewVector;
 
SamplerState textureSampler = sampler_state {
    Texture = (ModelTexture);
    MinFilter = Linear;
    MagFilter = Linear;
    AddressU = Clamp;
    AddressV = Clamp;
};
 
struct VertexShaderInput
{
    float4 Position : SV_POSITION;
    float4 Normal : NORMAL;
    float2 TextureCoordinate : TEXCOORD;
};
 
struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float4 Color : COLOR;
    float3 Normal : TEXCOORD;
    float2 TextureCoordinate : TEXCOORD1;
};
 
VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
 
    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
 
    float4 normal = normalize(mul(input.Normal, WorldInverseTranspose));
    float lightIntensity = dot(normal, DiffuseLightDirection);
	//float lightIntensity = 1;
    output.Color = saturate(DiffuseColor * DiffuseIntensity * lightIntensity);
 
    output.Normal = normal;
 
    output.TextureCoordinate = input.TextureCoordinate;
    return output;
}
 
float4 PixelShaderFunction(VertexShaderOutput input) : SV_Target
{
    //float3 light = normalize(DiffuseLightDirection);
    //float3 normal = normalize(input.Normal);
    //float3 r = normalize(2 * dot(light, normal) * normal - light);
    //float3 v = normalize(mul(normalize(ViewVector), World));
    //float dotProduct = dot(r, v);
 
    //float4 specular = SpecularIntensity * SpecularColor * max(pow(dotProduct, Shininess), 0) * length(input.Color);
 
    float4 textureColor = ModelTexture.Sample(textureSampler, input.TextureCoordinate);
	//float4 textureColor = ModelTexture.Sample();
	//float4 textureColor = float4(1, 0, 0, 1);
    textureColor.a = 1;
 
	//return textureColor;
    //return saturate(textureColor * (input.Color) + AmbientColor * AmbientIntensity) + 0.5 * textureColor;
return saturate(textureColor * (input.Color) + AmbientColor * AmbientIntensity) + 0.7 * textureColor;
}
 
technique Textured
{
    pass Pass1
    {
		Profile = 9.1;
        VertexShader = VertexShaderFunction;
        PixelShader = PixelShaderFunction;
    }
}