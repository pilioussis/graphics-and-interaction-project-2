﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit;
using SharpDX;
namespace Project2
{
    // Enemy class
    // Basically just shoots randomly, see EnemyController for enemy movement.
    using SharpDX.Toolkit.Graphics;
    class Ground : GameObject
    {
        Vector3 size;
        public Ground(Project2Game game, Vector3 pos, Vector3 size)
        {
            this.game = game;
            this.size = size;
            this.pos = pos;
            type = GameObjectType.City;
            myModel = game.assets.CreateTexturedSquare("textures/ground.jpg", pos, size);
            world = Matrix.Translation(pos);
            this.effect = game.Content.Load<Effect>("GroundShader");

            GetParamsFromModel();
        }

        public override void Draw(GameTime gametime)
        {
            GetParamsFromModel2();
            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(0, myModel.vertices, myModel.vertexStride);
            game.GraphicsDevice.SetVertexInputLayout(myModel.inputLayout);

            // Apply the basic effect technique and draw the object
            Matrix WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
            effect.Parameters["World"].SetValue(world);
            effect.Parameters["Projection"].SetValue(game.camera.Projection);
            effect.Parameters["View"].SetValue(game.camera.View);
            effect.Parameters["WorldInverseTranspose"].SetValue(WorldInverseTranspose);
            effect.Parameters["ModelTexture"].SetResource(myModel.Texture);
            effect.CurrentTechnique.Passes[0].Apply();

            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, myModel.vertices.ElementCount);
        }


    }
}
