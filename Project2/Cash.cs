﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using System.Threading.Tasks;
using SharpDX.Toolkit.Input;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace Project2
{
    class Cash : GameObject
    {
        Model model;

        float rotation = 0;
        private readonly String[] typeName = {"5cent", "10cent", "20Cent", "1dollar", "2dollar", "5dollar", "10dollar", "20dollar", "50dollar", "100dollar"};
        private readonly float[] amount = { .05f, .1f, .2f, 1, 2, 5, 10, 20, 50, 100 };
        private int denominationIndex;
        float scaleFactor = 1;
        public Cash(Project2Game game, Vector3 pos, int typeGiven)
        {
            //Coin type given
           

            this.game = game;
            this.pos = pos;
            world = Matrix.Translation(pos);
            rotation = game.random.NextFloat(0, 30);

            if (typeGiven == 1)
            {
                model = game.Content.Load<Model>("5cent");
                scaleFactor = 0.3f;
            }
            else if (typeGiven == 2){
                model = game.Content.Load<Model>("10cent");
                scaleFactor = 0.4f;
            }
            else if (typeGiven == 3){
                model = game.Content.Load<Model>("20cent");
                scaleFactor = 0.5f;
            }
            else if (typeGiven == 4)
            {
                model = game.Content.Load<Model>("1dollar");
                scaleFactor = 0.4f;
            }
            else if (typeGiven == 5)
            {
                model = game.Content.Load<Model>("2dollar");
                scaleFactor = 0.5f;
            }
            else if (typeGiven == 6){
                model = game.Content.Load<Model>("5dollar");
                scaleFactor = 1.3f;

            }
            else if (typeGiven == 7){
                model = game.Content.Load<Model>("10dollar");
                scaleFactor = 1.4f;
            }
            else if (typeGiven == 8){
                model = game.Content.Load<Model>("20dollar");
                scaleFactor = 1.5f;
            }
            else if (typeGiven == 9){
                model = game.Content.Load<Model>("50dollar");
                scaleFactor = 1.6f;
            }
            else {
                model = game.Content.Load<Model>("100dollar");
                scaleFactor = 1.8f;
            }

            denominationIndex = typeGiven-1;
            //BasicEffect.EnableDefaultLighting(model, true);

            this.effect = game.Content.Load<Effect>("CashShader");

        }
        public override void Draw(GameTime gametime)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    MaterialCollection test = model.Materials;
                    Material test1 = test.ElementAt(1);
                    //test1.GetProperty<MaterialKeys.DiffuseTexture>;
                    //effect.Parameters["ModelTexture"].SetResource(myModel.Texture);
                    Matrix WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
                    effect.Parameters["World"].SetValue(world);
                    effect.Parameters["Projection"].SetValue(game.camera.Projection);
                    effect.Parameters["View"].SetValue(game.camera.View);
                    effect.Parameters["WorldInverseTranspose"].SetValue(WorldInverseTranspose);
                    effect.Parameters["ModelTexture"].SetResource(test1.GetProperty(MaterialKeys.DiffuseTexture).ElementAt(0).Texture);
                    part.Effect = effect;
                }
                mesh.Draw(game.GraphicsDevice);
            }

            // Some objects such as the Enemy Controller have no model and thus will not be drawn
            model.Draw(game.GraphicsDevice, world, game.camera.View, game.camera.Projection);
        }


        public override void Update(GameTime gameTime)
        {
            Player player = ((Player)game.gameObjects[1]);
            List<BuildingBlock> buildings = ((City)game.gameObjects[0]).buildings;

            wiggle((float)gameTime.TotalGameTime.TotalSeconds);

            //If a player touches the cash.
            if (Vector3.Distance(player.pos, this.pos) < 2 * player.getBallRadius())
            {
                game.Remove(this);
                player.addCash(amount[denominationIndex]);
            }

            world = Matrix.Scaling(scaleFactor) * Matrix.RotationY(rotation) * Matrix.Translation(pos);
        }

        public void wiggle(float displacement)
        {
            pos.Y += .03f * (float)Math.Sin(4 * displacement);
            rotation = displacement * 4;
        }
    }
}
