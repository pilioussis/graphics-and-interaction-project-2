﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;

namespace Project2
{
    using SharpDX.Toolkit.Graphics;
    abstract public class Shape
    {
        public BasicEffect basicEffect;
        public VertexInputLayout inputLayout;
		public Buffer<VertexPositionColor> vertices;
        public Project2Game game;

        public abstract void Update(GameTime gametime);
        public abstract void Draw(GameTime gametime);
    }
}
