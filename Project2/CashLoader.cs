﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX.Toolkit;
using System.Threading.Tasks;
using SharpDX.Toolkit.Input;
using SharpDX.Toolkit.Graphics;
using SharpDX;

namespace Project2
{
    class CashLoader : GameObject
    {
        readonly private float RAINBOW_STEP = 1f;
        private float noteHeight;
        readonly private int NUM_NOTES = 5;
        public CashLoader(Project2Game game)
        {
            this.game = game;
            Vector3 currPos;
            noteHeight = (City.heightMax - City.heightMin)/NUM_NOTES;
            List<BuildingBlock> buildings = ((City)game.gameObjects[0]).buildings;
            for (int i = 0; i < buildings.Count; i++)
            {
                if (game.random.Next(0, 13) == 1 && i != buildings.Count - 1)
                {
                    makeRainbow(buildings[i], buildings[i + 1]);

                }

                if (game.random.Next(0, 13) == 1 && i < buildings.Count - City.buildingsZ)
                {
                    makeRainbow(buildings[i], buildings[i + City.buildingsZ]);
                }

                if (game.random.Next(0, 2) == 1)
                {
                    currPos = new Vector3(buildings[i].pos.X + buildings[i].size.X / 2,
                                            buildings[i].size.Y + 2,
                                            buildings[i].pos.Z + buildings[i].size.Z / 2);

                    game.gameObjects.Add(new Cash(game, currPos, selectNote(buildings[i].size.Y)));
                }


            }
        }

        public void makeRainbow(BuildingBlock from, BuildingBlock to)
        {
            float steep = game.random.Next(1, 5);
            float steepness = Math.Abs((from.size.Y - to.size.Y) / 100);
            float distanceXZ = (float)Math.Sqrt(((to.pos.X + to.size.X) - (from.pos.X + from.size.X)) + (to.pos.Z + to.size.Z) - (from.pos.Z + from.size.Z));
            int numCoins = (int)Math.Round(distanceXZ*steep/2 / RAINBOW_STEP);

            Vector3 step = new Vector3(((to.pos.X + to.size.X / 2) - (from.pos.X + from.size.X / 2)) / numCoins,
                                       ((to.pos.Y + to.size.Y) - (from.pos.Y + from.size.Y)) / numCoins,
                                       ((to.pos.Z + to.size.Z / 2) - (from.pos.Z + from.size.Z / 2)) / numCoins);


            
            float x, y;
            for (int i = 0; i < numCoins + 1; i++)
            {
                x = ((i / (float)numCoins) * 4) - 2;
                y = -x * x + 4;
                y *= steep;


                Vector3 coinPos = new Vector3(from.pos.X + (from.size.X) / 2 + i * step.X,
                                                from.pos.Y + from.size.Y + i * step.Y + y + 6,
                                                from.pos.Z + (from.size.Z) / 2 + i * step.Z);
                int typeGiven = game.random.Next(1, 6);
                game.gameObjects.Add(new Cash(game, coinPos,typeGiven));
            }

        }

        private int selectNote(float height)
        {
            if (height < noteHeight)
            {
                return 6;
            }
            else if (height < noteHeight*2)
            {
                return 7;
            }
            else if (height < noteHeight*3)
            {
                return 8;
            }
            else if (height < noteHeight*4)
            {
                return 9;
            }
            else
            {
                return 10;
            }
        }
    }
}