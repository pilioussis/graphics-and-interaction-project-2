﻿// Copyright (c) 2010-2013 SharpDX - Alexandre Mutel
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using SharpDX;
using SharpDX.Toolkit;
using System;
using System.Collections.Generic;
using SharpDX.Toolkit.Input;
using Windows.Devices.Sensors;
using Windows.UI.Input;
using Windows.UI.Core;
using Windows.Storage;
namespace Project2
{
    // Use this namespace here in case we need to use Direct3D11 namespace as well, as this
    // namespace will override the Direct3D11.
    using SharpDX.Toolkit.Graphics;



    public class Project2Game : Game
    {

        StorageFolder locals;
        float highScore = 0;
        public Boolean wroteAlready;

        private GraphicsDeviceManager graphicsDeviceManager;
        public List<GameObject> gameObjects;
        public Boolean started = false;
        private Stack<GameObject> addedGameObjects;
        private Stack<GameObject> removedGameObjects;
        private KeyboardManager keyboardManager;
        public KeyboardState keyboardState;

        public float timeLeft;
        public float minutesLeft;
        public int secs = 0;
        public int score;
        public MainPage mainPage;

        // Represents the camera's position and orientation
        public Camera camera;
        public GameInput input;
        // Represents the player
        public Player player;
        // Graphics assets
        public Assets assets;

        public float boundaryLeft;
        public float boundaryRight;
        public float boundaryTop;
        public float boundaryBottom;

        //game time in milliseconds
        public readonly static float TOTAL_TIME = 60000;

        public Random random;
        public Boolean keyboardsOnly;
        private Model skybox;
        private Matrix skyboxWorld;
        /// <summary>
        /// Initializes a new instance of the <see cref="Lab3Game" /> class.
        /// </summary>
        public Project2Game(MainPage mainPage)
        {
            wroteAlready = false;
            locals = ApplicationData.Current.LocalFolder;

            //Try read high score
            highScore = 0;
            ReadScore();

            // Creates a graphics manager. This is mandatory.
            graphicsDeviceManager = new GraphicsDeviceManager(this);
            
            // Setup the relative directory to the executable directory
            // for loading contents with the ContentManager
            Content.RootDirectory = "Content";

            // Create the keyboard manager
            keyboardManager = new KeyboardManager(this);
            assets = new Assets(this);

            //Random Seed
            random = new Random();

            //Time left
            timeLeft = TOTAL_TIME;
            minutesLeft = (float)Math.Truncate(timeLeft / 60 / 1000);
            secs = (int)timeLeft / 1000 % 60;

            // Set boundaries.
            boundaryLeft = -4.5f;
            boundaryRight = 4.5f;
            boundaryTop = 4;
            boundaryBottom = -4.5f;

            this.mainPage = mainPage;

            score = 0;

            //check to see if accelerometer access fails, if so, then it is not a tablet, so use keyboard
            try
            {
                (new GameInput()).accelerometer.GetCurrentReading();
                keyboardsOnly = false;
            }
            catch (Exception e)
            {
                keyboardsOnly = true;
            }


        }

        async void ReadScore()
        {
            try
            {
                StorageFile scoreFile = await locals.GetFileAsync("hunnedsFile.txt");
                String hS = await FileIO.ReadTextAsync(scoreFile);
                highScore = float.Parse(hS);
                // Data is contained in highScore
            }
            catch (Exception)
            {
                highScore = 0;
                //WriteScore();
                // hunnedsFile not found

            }
        }

        async void WriteScore()
        {

            StorageFile scoreFile = await locals.CreateFileAsync("hunnedsFile.txt",
                CreationCollisionOption.ReplaceExisting);
            using (var fs = await scoreFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                var outStream = fs.GetOutputStreamAt(0);
                var dataWriter = new Windows.Storage.Streams.DataWriter(outStream);
                dataWriter.WriteString(highScore.ToString());
                await dataWriter.StoreAsync();
                dataWriter.DetachStream();
                await outStream.FlushAsync();
                outStream.Dispose(); // 
                fs.Dispose();
            }
            //await FileIO.WriteTextAsync(scoreFile, highScore.ToString());
        }

        public void ResetScore()
        {
            highScore = 0;
            WriteScore();
        }

        protected override void LoadContent()
        {
            //Load the skybox model
            skybox = Content.Load<Model>("Skybox");
            skyboxWorld = Matrix.Scaling(150*20) * Matrix.Translation(City.WIDTH/2, 0, City.WIDTH/2);

            // Initialise game object containers.
            gameObjects = new List<GameObject>();
            addedGameObjects = new Stack<GameObject>();
            removedGameObjects = new Stack<GameObject>();
            
            gameObjects.Add(new City(this));
            gameObjects.Add(new Player(this, new Vector3(City.WIDTH/6+5, City.heightMax*2, City.WIDTH/6+5), new Vector3(3,3,3)));
            player = (Player)gameObjects[1];
            CashLoader cashLoader = new CashLoader(this);

            base.LoadContent();
        }

        protected override void Initialize()
        {
            Window.Title = "Project2";
            camera = new Camera(this);

            base.Initialize();
        }

        protected override void Update(GameTime gameTime)
        {
            if (started)
            {
                timeLeft -= gameTime.ElapsedGameTime.Milliseconds;

                updateTimerAndCash();
                keyboardState = keyboardManager.GetState();
                flushAddedAndRemovedGameObjects();
                camera.Update();

                for (int i = 0; i < gameObjects.Count; i++)
                {
                    gameObjects[i].Update(gameTime);
                }
            }
            else
            {
                mainPage.ShowHighScore(highScore.ToString());
            }
            if (keyboardState.IsKeyDown(Keys.Escape))
            {
                this.Exit();
                this.Dispose();
            }
            // Handle base.Update
            base.Update(gameTime);

            //End game and write high score if higher
            if (timeLeft <= 0)
            {
                if (player.getCash() >= highScore)
                {
                    updateTimerAndCash();
                }
                if (!wroteAlready)
                {
                    WriteScore();
                    wroteAlready = true;
                }
                mainPage.EndGame();
            }


        }

        protected override void Draw(GameTime gameTime)
        {
            // Clears the screen with the Color.CornflowerBlue
            GraphicsDevice.Clear(Color.Black);

            //Draw the skybox
            //BoundingSphere skyboxBounds = skybox.CalculateBounds();
            //var scaling = (City.widthX / skyboxBounds.Radius) * 100;
            //Vector3 pos = new Vector3(1,1,1);
            skybox.Draw(GraphicsDevice, skyboxWorld, camera.View, camera.Projection);

            for (int i = 0; i < gameObjects.Count; i++)
            {
                gameObjects[i].Draw(gameTime);
            }
            // Handle base.Draw
            base.Draw(gameTime);
        }

        private void updateTimerAndCash()
        {
            //It seems do be out by 6 seconds for some reason, the "-.006" term here adjusts for that
            minutesLeft = (float)Math.Truncate(timeLeft / 60 / 1000 - .006);
            secs = (int)timeLeft / 1000 % 60;
            //The first two statements of this (minutes and seconds) are mainly just "for show"
            mainPage.showRandomString(minutesLeft.ToString() + ":" + secs.ToString() + "." + (.001f*timeLeft%1).ToString("n2").Substring(2,2));
            mainPage.ShowCash(player.getCash().ToString("n2"));
            mainPage.ShowHighScore(highScore.ToString());
            if (highScore < player.getCash())
            {
                highScore = player.getCash();
                mainPage.UpdateHighScore(player.getCash().ToString());
            }
            
        }

        // Count the number of game objects for a certain type.
        public int Count(GameObjectType type)
        {
            int count = 0;
            foreach (var obj in gameObjects)
            {
                if (obj.type == type) { count++; }
            }
            return count;
        }

        // Add a new game object.
        public void Add(GameObject obj)
        {
            if (!gameObjects.Contains(obj) && !addedGameObjects.Contains(obj))
            {
                addedGameObjects.Push(obj);
            }
        }

        // Remove a game object.
        public void Remove(GameObject obj)
        {
            if (gameObjects.Contains(obj) && !removedGameObjects.Contains(obj))
            {
                removedGameObjects.Push(obj);
            }
        }

        // Process the buffers of game objects that need to be added/removed.
        private void flushAddedAndRemovedGameObjects()
        {
            while (addedGameObjects.Count > 0) { gameObjects.Add(addedGameObjects.Pop()); }
            while (removedGameObjects.Count > 0) { gameObjects.Remove(removedGameObjects.Pop()); }
        }

    }
}
