﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;
namespace Project2
{
    using SharpDX.Toolkit.Graphics;
    public class Assets
    {
        Project2Game game;

        public Assets(Project2Game game)
        {
            this.game = game;
        }

        // Dictionary of currently loaded models.
        // New/existing models are loaded by calling GetModel(modelName, modelMaker).
        public Dictionary<String, MyModel> modelDict = new Dictionary<String, MyModel>();

        // Load a model from the model dictionary.
        // If the model name hasn't been loaded before then modelMaker will be called to generate the model.
        public delegate MyModel ModelMaker();
        public MyModel GetModel(String modelName, ModelMaker modelMaker)
        {
            if (!modelDict.ContainsKey(modelName))
            {
                modelDict[modelName] = modelMaker();
            }
            return modelDict[modelName];
        }


        public MyModel CreateColouredCube(Vector3 size, Color color)
        {
            Color color2 = new Color();
            color2.R = (byte) ( color.R + 10);
            color2.G = (byte)(color.G + 10);
            color2.B = color.B;
            Color color3 = new Color();
            color3.R = (byte)(color.R - 10);
            color3.G = (byte)(color.G - 10);
            color3.B = (byte)(color.B + 10);
            Color color4 = new Color();
            color4.R = (byte)(color.R + 30);
            color4.G = (byte)(color.G - 5);
            color4.B = (byte)(color.B + 5);
            Color color5 = new Color();
            color5.R = (byte)(color.R - 5);
            color5.G = (byte)(color.G + 20);
            color5.B = (byte)(color.B + 10);
            Color color6 = new Color();
            color6.R = (byte)(color.R - 20);
            color6.G = (byte)(color.G - 10);
            color6.B = (byte)(color.B - 10);

            VertexPositionColor[] shapeArray = new VertexPositionColor[]{
            new VertexPositionColor(new Vector3(0f, 0f, 0f), color), // Front
            new VertexPositionColor(new Vector3(0f, 1.0f, 0f), color),
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 0f), color),
            new VertexPositionColor(new Vector3(0f, 0f, 0f), color),
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 0f), color),
            new VertexPositionColor(new Vector3(1.0f, 0f, 0f), color),

            
            new VertexPositionColor(new Vector3(0f, 0f, 1.0f), color2), // BACK
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 1.0f), color2),
            new VertexPositionColor(new Vector3(0f, 1.0f, 1.0f), color2),
            new VertexPositionColor(new Vector3(0f, 0f, 1.0f), color2),
            new VertexPositionColor(new Vector3(1.0f, 0f, 1.0f), color2),
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 1.0f), color2),

            new VertexPositionColor(new Vector3(0f, 1.0f, 0f),color3), // Top
            new VertexPositionColor(new Vector3(0f, 1.0f, 1.0f), color3),
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 1.0f), color3),
            new VertexPositionColor(new Vector3(0f, 1.0f, 0f), color3),
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 1.0f), color3),
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 0f),color3),

            new VertexPositionColor(new Vector3(0f, 0f, 0f),color4), // Bottom
            new VertexPositionColor(new Vector3(1.0f, 0f, 1.0f), color4),
            new VertexPositionColor(new Vector3(0f, 0f, 1.0f), color4),
            new VertexPositionColor(new Vector3(0f, 0f, 0f),color4),
            new VertexPositionColor(new Vector3(1.0f, 0f, 0f), color4),
            new VertexPositionColor(new Vector3(1.0f, 0f, 1.0f),color4),

            new VertexPositionColor(new Vector3(0f, 0f, 0f), color5), // Left
            new VertexPositionColor(new Vector3(0f, 0f, 1.0f), color5),
            new VertexPositionColor(new Vector3(0f, 1.0f, 1.0f), color5),
            new VertexPositionColor(new Vector3(0f, 0f, 0f), color5),
            new VertexPositionColor(new Vector3(0f, 1.0f, 1.0f), color5),
            new VertexPositionColor(new Vector3(0f, 1.0f, 0f),color5),

            new VertexPositionColor(new Vector3(1.0f, 0f, 0f), color6), // Right
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 1.0f), color6),
            new VertexPositionColor(new Vector3(1.0f, 0f, 1.0f),color6),
            new VertexPositionColor(new Vector3(1.0f, 0f, 0f), color6),
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 0f), color6),
            new VertexPositionColor(new Vector3(1.0f, 1.0f, 1.0f),color6),
            };

            for (int i = 0; i < shapeArray.Length; i++)
            {
                shapeArray[i].Position.X *= size.X / 2;
                shapeArray[i].Position.Y *= size.Y / 2;
                shapeArray[i].Position.Z *= size.Z / 2;
            }

            return new MyModel(game, shapeArray);
        }


        public MyModel CreateColouredGround(Vector3 pos, Vector3 size,Color color) {
            VertexPositionColor[] shapeArray = new VertexPositionColor[]{

            new VertexPositionColor(new Vector3(pos.X           , pos.Y,     pos.Z),            color), // Top
            new VertexPositionColor(new Vector3(pos.X           , pos.Y,     pos.Z + size.Z),   color),
            new VertexPositionColor(new Vector3(pos.X + size.X  , pos.Y,     pos.Z + size.Z),   color),
            new VertexPositionColor(new Vector3(pos.X           , pos.Y,     pos.Z),            color),
            new VertexPositionColor(new Vector3(pos.X + size.X  , pos.Y,     pos.Z + size.Z),   color),
            new VertexPositionColor(new Vector3(pos.X + size.X  , pos.Y,     pos.Z),            color),
            };

            

            return new MyModel(game, shapeArray);
        }
        public MyModel CreateTexturedWalls(String texturePath, Vector3 pos, Vector3 size)
        {
            Vector3 frontNormal = new Vector3(0.0f, 0.0f, -1.0f);
            Vector3 backNormal = new Vector3(0.0f, 0.0f, 1.0f);
            Vector3 leftNormal = new Vector3(-1.0f, 0.0f, 0.0f);
            Vector3 rightNormal = new Vector3(1.0f, 0.0f, 0.0f);

            VertexPositionNormalTexture[] shapeArray = new VertexPositionNormalTexture[]{
           
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y         , pos.Z+ size.Z),   frontNormal, new Vector2(1, 1)), // Front
            new VertexPositionNormalTexture(new Vector3(pos.X+ size.X , pos.Y         , pos.Z+ size.Z),   frontNormal, new Vector2(0, 1)),
            new VertexPositionNormalTexture(new Vector3(pos.X+ size.X , pos.Y+size.Y  , pos.Z+ size.Z),   frontNormal, new Vector2(0, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y         , pos.Z+ size.Z),   frontNormal, new Vector2(1, 1)),
            new VertexPositionNormalTexture(new Vector3(pos.X+ size.X , pos.Y+ size.Y , pos.Z+ size.Z),   frontNormal, new Vector2(0, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y+ size.Y , pos.Z+ size.Z),   frontNormal, new Vector2(1, 0)),
           
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y         , pos.Z),           backNormal, new Vector2(0, 1)), // BACK
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y+size.Y  , pos.Z),           backNormal, new Vector2(0, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  , pos.Y+size.Y  , pos.Z),           backNormal, new Vector2(1, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y         , pos.Z),           backNormal, new Vector2(0, 1)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  ,pos.Y+size.Y   , pos.Z),           backNormal, new Vector2(1, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  , pos.Y         , pos.Z),           backNormal, new Vector2(1, 1)),
          
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y         , pos.Z),           leftNormal, new Vector2(1, 1)), // Left
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y         , pos.Z+size.Z),    leftNormal, new Vector2(0, 1)),
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y+size.Y  , pos.Z+size.Z),    leftNormal, new Vector2(0, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y         , pos.Z),           leftNormal, new Vector2(1, 1)),
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y+size.Y  , pos.Z+size.Z),    leftNormal, new Vector2(0, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X         , pos.Y+size.Y  , pos.Z),           leftNormal, new Vector2(1, 0)),
                 

            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  , pos.Y         , pos.Z+size.Z),   rightNormal, new Vector2(1, 1)), // Right
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  , pos.Y+size.Y  , pos.Z),           rightNormal, new Vector2(0, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  , pos.Y+size.Y  , pos.Z+size.Z),    rightNormal, new Vector2(1, 0)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  , pos.Y         , pos.Z+size.Z),    rightNormal, new Vector2(1, 1)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  , pos.Y         , pos.Z),           rightNormal, new Vector2(0, 1)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X  , pos.Y+size.Y  , pos.Z),           rightNormal, new Vector2(0, 0)),
            
            };

            return new MyModel(game, shapeArray, texturePath);
        }
        public MyModel CreateTexturedSquare(String texturePath, Vector3 pos, Vector3 size)
        {
            Vector3 topNormal = new Vector3(0.0f, 1.0f, 0.0f);

            VertexPositionNormalTexture[] shapeArray = new VertexPositionNormalTexture[]{
            new VertexPositionNormalTexture(new Vector3(pos.X, pos.Y+size.Y, pos.Z), topNormal, new Vector2(0.0f, 1.0f)), // Top
            new VertexPositionNormalTexture(new Vector3(pos.X, pos.Y+size.Y, pos.Z+ size.Z), topNormal, new Vector2(0.0f, 0.0f)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X, pos.Y+size.Y, pos.Z+ size.Z), topNormal, new Vector2(1.0f, 0.0f)),
            new VertexPositionNormalTexture(new Vector3(pos.X, pos.Y+size.Y, pos.Z), topNormal, new Vector2(0.0f, 1.0f)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X, pos.Y+size.Y, pos.Z+size.Z), topNormal, new Vector2(1.0f, 0.0f)),
            new VertexPositionNormalTexture(new Vector3(pos.X+size.X, pos.Y+size.Y, pos.Z), topNormal, new Vector2(1.0f, 1.0f)),
            };

            return new MyModel(game, shapeArray, texturePath);
        }
    }
}
