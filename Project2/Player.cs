﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpDX;
using SharpDX.Toolkit;
using SharpDX.Toolkit.Input;
using SharpDX.Toolkit.Graphics;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Input;
using Windows.Devices.Sensors;
using Windows.UI.Xaml.Input;

namespace Project2
{
    public class Player : GameObject
    {
        Model model;
        AccelerometerReading accelerometerReading;
        public GameInput input;

        //Constants
        private readonly static float FRICTION = .003f;
        private readonly static float GRAVITY = .009f;
        private readonly static float BALLRADIUS = 1f;
        private readonly static float JUMPHEIGHT = .75f;
        private readonly static float CHARGE_SPEED = .07f;
        private readonly static float AIRCONTROL = .2f;
        public  readonly static float ACCELERATION = .02f;
        public readonly static float SPEEDLIMIT = .4f;
        public readonly static float TURNSPEED = 0.02f;
        public readonly float MAXJUMPPOWER = .8f;
        private readonly float DEAD_ZONE1 = 0.1f; //Accelerometer reading ignore threshold
        private readonly float DEAD_ZONE2 = 0.2f;
        private readonly float DEAD_ZONE3 = 0.4f;
        private readonly float MOVE_DEAD_ZONE = -0.5f;

        //Variables
        public float jumpPower = 0;
        public float direction = (float)Math.PI/4;
        public static float speed = 0;
        private float velocityY = 0;
        private Boolean onGround = true;
        private float bank;

        public Boolean charging;
        public float chargeAmount;

        float newX;
        float newY;
        float newZ;

        public Player(Project2Game game, Vector3 pos, Vector3 size)
        {
            charging = false;
            bank = 0;
            this.game = game;
            this.pos = pos;
            model = game.Content.Load<Model>("Sphere");
            this.effect = game.Content.Load<Effect>("SphereShader");

            input = new GameInput();
            
            //If we are not using the keyboard, set up accelerometer
            if(!game.keyboardsOnly)
                accelerometerReading = input.accelerometer.GetCurrentReading();
        }


        public override void Draw(GameTime gametime)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect;
                }
                mesh.Draw(game.GraphicsDevice);
            }
        }

        public override void Update(GameTime gameTime)
        {
            //get updates from keyboard or sensors
            if (game.keyboardsOnly)
                updateKeyboardValues();
            else  
                updateTabletValues();
            
            //check for blocking and apply physics constants
            movePlayer();
            applyGravity();
            applyFriction();

            //created worlk matrix and update shader info.
            world = Matrix.Scaling(new Vector3(1, 1 - jumpPower, 1))*
                    Matrix.Translation(new Vector3(0, -jumpPower/MAXJUMPPOWER*BALLRADIUS,0))*
                    Matrix.Translation(pos);

            updateTextureShader();
        }

        private void updateTextureShader()
        {
            Matrix WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
            effect.Parameters["World"].SetValue(world);
            effect.Parameters["Projection"].SetValue(game.camera.Projection);
            effect.Parameters["View"].SetValue(game.camera.View);
            effect.Parameters["worldInvTrp"].SetValue(WorldInverseTranspose);
            effect.Parameters["cameraPos"].SetValue(game.camera.cameraPos);
        }

        private void updateTabletValues()
        {
            accelerometerReading = input.accelerometer.GetCurrentReading();
            if (charging)
            {
                if (jumpPower < MAXJUMPPOWER)
                {
                    jumpPower += chargeAmount;
                }
                if (jumpPower > MAXJUMPPOWER)
                {
                    jumpPower = MAXJUMPPOWER;
                }
            }

            if (!charging && jumpPower != 0)
            {
                if (onGround)
                    velocityY = (JUMPHEIGHT * jumpPower*1.3f);
                onGround = false;

                jumpPower = 0;
            }

            if(accelerometerReading.AccelerationX < -DEAD_ZONE3) {
                direction += TURNSPEED*2;
            }
            else if (accelerometerReading.AccelerationX > DEAD_ZONE3)
            {
                direction -= TURNSPEED*2;
            }

            else if (accelerometerReading.AccelerationX < -DEAD_ZONE2)
            {
                direction += TURNSPEED;
            }
            else if (accelerometerReading.AccelerationX > DEAD_ZONE2)
            {
                direction -= TURNSPEED;
            }

            else if (accelerometerReading.AccelerationX < -DEAD_ZONE1)
            {
                direction += TURNSPEED/2;
            }
            else if (accelerometerReading.AccelerationX > DEAD_ZONE1)
            {
                direction -= TURNSPEED/2;
            }

            if (accelerometerReading.AccelerationY > MOVE_DEAD_ZONE)
            {
                if (onGround)
                    speed = (float) (accelerometerReading.AccelerationY-  MOVE_DEAD_ZONE)*1.7f;
                else
                    speed = (float)(accelerometerReading.AccelerationY - MOVE_DEAD_ZONE) * AIRCONTROL * 1.7f;
            }

            if (accelerometerReading.AccelerationY < MOVE_DEAD_ZONE)
            {
                speed *= 0.8f;
            }
            if (speed > SPEEDLIMIT)
            {
                speed = SPEEDLIMIT;
            }
        }

        private void movePlayer()
        {
            newY = pos.Y + velocityY;
            //System.Diagnostics.Debug.WriteLine(newY + "<---");
            newX = pos.X + speed * (float)Math.Cos(direction);
            newZ = pos.Z + speed * (float)Math.Sin(direction);

            //Check blocking downwards
            if (newY < pos.Y)
                if (blockedInYneg())
                {
                    velocityY = 0;
                    onGround = true;
                }
                else
                {
                    pos.Y = newY;
                }
            else
                pos.Y = newY;

            //Check Blocking in positive X direction
            if (newX > pos.X)
            {
                if (blockedInXpos())
                    speed *= (float) Math.Abs(Math.Sin(direction));
                else
                    pos.X = newX;
            }

            //Check blocking in the negative X direction
            if (newX < pos.X)
            {
                if (blockedInXneg())
                    speed *= (float)Math.Abs(Math.Sin(direction));
                else
                    pos.X = newX;
            }

            //Check blocking in the positive Z direction
            if (newZ > pos.Z)
            {
                if (blockedInZpos())
                    speed *= (float)Math.Abs(Math.Cos(direction));
                else
                    pos.Z = newZ;
            }
            
            //Check blocking in the negative Z direction
            if (newZ < pos.Z)
            {
                if (blockedInZneg())
                    speed *= (float)Math.Abs(Math.Cos(direction));
                else
                    pos.Z = newZ;
            }
            
        }

        private void applyGravity()
        {
            velocityY -= GRAVITY;
        }

        private void applyFriction()
        {
            if (speed >= FRICTION)
                speed -= FRICTION;
            else if (speed <= -FRICTION)
                speed += FRICTION;
            else
                speed = 0;
        }

        //get input from keyboard
        private void updateKeyboardValues()
        {
            if (game.keyboardState.IsKeyDown(Keys.Space))
            {
                if (jumpPower < MAXJUMPPOWER)
                {
                    jumpPower += CHARGE_SPEED;
                }
            }

            if (game.keyboardState.IsKeyUp(Keys.Space) && jumpPower != 0)
            {
                if (onGround)
                    velocityY = (JUMPHEIGHT * jumpPower);
                onGround = false;

                jumpPower = 0;
            }
            if (game.keyboardState.IsKeyDown(Keys.Up))
            {
                if (onGround)
                {
                    if (speed < SPEEDLIMIT)
                        speed += ACCELERATION;
                }
                else
                {
                    if (speed < SPEEDLIMIT)
                        speed += ACCELERATION * AIRCONTROL;
                }
            }
            if (game.keyboardState.IsKeyDown(Keys.Down))
            {
                if (onGround)
                {
                    if (speed < SPEEDLIMIT)
                        speed -= ACCELERATION;
                }
                else
                {
                    if (speed < SPEEDLIMIT)
                        speed -= ACCELERATION * AIRCONTROL;
                }
            }
            if (game.keyboardState.IsKeyDown(Keys.Left))
            {
                direction += TURNSPEED;
            }
            if (game.keyboardState.IsKeyDown(Keys.Right))
            {
                direction -= TURNSPEED;
            }
            if (game.keyboardState.IsKeyUp(Keys.Left) && game.keyboardState.IsKeyUp(Keys.Right) && !game.keyboardsOnly)
            {
                direction -= (float)accelerometerReading.AccelerationX * TURNSPEED;
            }

            if (game.keyboardState.IsKeyUp(Keys.Up) && game.keyboardState.IsKeyUp(Keys.Down) && !game.keyboardsOnly)
            {
                if (Math.Abs(speed) < SPEEDLIMIT)
                    speed = (float)accelerometerReading.AccelerationY * SPEEDLIMIT;
            }
        }

        //helper for blocking
        private float dist(float x1, float x2, float y1, float y2)
        {
            float temp = ((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1));
            float dist = (float)Math.Sqrt(temp) - BALLRADIUS;
            return dist;
        }


        public float getBallRadius()
        {
            return BALLRADIUS;
        }

        public void addCash(float amount)
        {
            bank += amount;
        }

        public float getCash()
        {
            return bank;
        }

        public void resetCash()
        {
            bank = 0;
        }


        //NOTE the below functions are not commented as they are all very similar with only subtle changes.

        //blocking in the y direction (up and down)
        private Boolean blockedInYneg() {

            //get the list of buildings
            List<BuildingBlock> buildings = ((City)game.gameObjects[0]).buildings;
            float maxHeight = 0;
            int aboveBuilding = -1;

            //ground blocking
            if (pos.Y < 0 + BALLRADIUS)
            {
                pos.Y = 0 + BALLRADIUS;
                return true;

            }
            //iterate through each building.
            for (int i = 0; i < buildings.Count; i++)
            {           
                //if withing X and Z bounds
                if (newX > buildings[i].pos.X && newX < buildings[i].pos.X + buildings[i].size.X && 
                    newZ > buildings[i].pos.Z && newZ < buildings[i].pos.Z + buildings[i].size.Z 
                   )
                {       //if within Y bounds
                        if (maxHeight < buildings[i].size.Y)
                        {
                            //record Y direction
                            maxHeight = buildings[i].size.Y;
                            aboveBuilding = i;
                        }                       
                    
                }
            }
            //check if above recorded Y direction and implement blocking
            if (aboveBuilding != -1 && newY - BALLRADIUS < maxHeight)
            {
                pos.Y = maxHeight + BALLRADIUS;

                return true;
            }
            return false;        
        }

        private Boolean blockedInXpos()
        {           
            List<BuildingBlock> buildings = ((City)game.gameObjects[0]).buildings;
            float minDistance = 9999;
            int closestBuilding = -1;

            for (int i = 0; i < buildings.Count; i++)
            {
                if (newZ > buildings[i].pos.Z - BALLRADIUS && newZ < buildings[i].pos.Z + buildings[i].size.Z + BALLRADIUS &&
                    newY < buildings[i].size.Y + BALLRADIUS)
                {
                    //face
                    if (buildings[i].pos.X  - BALLRADIUS >= pos.X)
                    {
                        if (buildings[i].pos.X < minDistance)
                        {
                            minDistance = buildings[i].pos.X;
                            closestBuilding = i;
                        }
                    }
               
                }
            }         
           
            if (closestBuilding != -1 && newX >= buildings[closestBuilding].pos.X -BALLRADIUS)
            {              
                return true;
            }
            return false;
        }

        private Boolean blockedInXneg()
        {
            List<BuildingBlock> buildings = ((City)game.gameObjects[0]).buildings;
            float maxDistance = -9999;
            int closestBuilding = -1;
            
            for (int i = 0; i < buildings.Count; i++)
            {
                if (newZ > buildings[i].pos.Z -BALLRADIUS && newZ < buildings[i].pos.Z + buildings[i].size.Z + BALLRADIUS &&
                    newY < buildings[i].size.Y + BALLRADIUS)
                {

                    if (buildings[i].pos.X + buildings[i].size.X + BALLRADIUS <= pos.X)
                    {
                        if (buildings[i].pos.X + buildings[i].size.X > maxDistance)
                        {
                            maxDistance = buildings[i].pos.X;
                            closestBuilding = i;
                        }
                    }
                }
            }

            if (closestBuilding != -1 && newX <= buildings[closestBuilding].pos.X + buildings[closestBuilding].size.X + BALLRADIUS)
            {
                return true;
            }
            return false;
        }

        private Boolean blockedInZpos()
        {
            List<BuildingBlock> buildings = ((City)game.gameObjects[0]).buildings;
            float minDistance = 9999;
            int closestBuilding = -1;
            for (int i = 0; i < buildings.Count; i++)
            {
                if (newX > buildings[i].pos.X - BALLRADIUS && newX < buildings[i].pos.X + buildings[i].size.X + BALLRADIUS&&
                    newY < buildings[i].size.Y + BALLRADIUS)
                {
                    if (buildings[i].pos.Z - BALLRADIUS >= pos.Z)
                    {
                        if (buildings[i].pos.Z < minDistance)
                        {
                            minDistance = buildings[i].pos.Z;
                            closestBuilding = i;
                        }
                    }
                }
            }
            if (closestBuilding != -1 && newZ >= buildings[closestBuilding].pos.Z - BALLRADIUS)
            {
                return true;
            }
            return false;
        }
        private Boolean blockedInZneg()
        {
            List<BuildingBlock> buildings = ((City)game.gameObjects[0]).buildings;
            float maxDistance = -9999;
            int closestBuilding = -1;

            for (int i = 0; i < buildings.Count; i++)
            {
                if (newX > buildings[i].pos.X - BALLRADIUS && newX < buildings[i].pos.X + buildings[i].size.X + BALLRADIUS &&
                    newY < buildings[i].size.Y + BALLRADIUS)
                {
                    if (buildings[i].pos.Z + buildings[i].size.Z + BALLRADIUS <= pos.Z)
                    {
                        if (buildings[i].pos.Z + buildings[i].size.Z > maxDistance)
                        {
                            maxDistance = buildings[i].pos.Z;
                            closestBuilding = i;
                        }
                    }
                }
            }
            if (closestBuilding != -1 && newZ <= buildings[closestBuilding].pos.Z + buildings[closestBuilding].size.Z + BALLRADIUS)
            {
                return true;
            }
            return false;
        }


       


    }
}