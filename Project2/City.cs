﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Toolkit;

namespace Project2
{
    using SharpDX.Toolkit.Graphics;

    

    class City : GameObject
    {

        public static readonly int WIDTH = 150;


        public static readonly int buildingsX = 10;
        public static readonly int buildingsZ = 10;

        public static readonly int heightMin = 5;
        public static readonly int heightMax = 55;

        private int shortTexture;
        private int midTexture;
        public List<BuildingBlock> buildings;
       

        public City(Project2Game game)
        {
            this.game = game;
            buildings = new List<BuildingBlock>();
            shortTexture = (heightMax-heightMin)/3 + heightMin;
            midTexture = (heightMax-heightMin)/3*2 + heightMin;
            makeGround();

            createBuildings();
            
        }

        private void makeGround()
        {
            float from = -((float) WIDTH);
            float to = ((float)WIDTH) ;
            float textureSize = 50;
            float width = (to - from) / textureSize;
            System.Diagnostics.Debug.WriteLine("width" + width);
            float currX = from;
            float currZ = from;

            for (int i = 0; i < width * width; i++)
            {
                game.Add(new Ground(game, new Vector3(currX, 0, currZ), new Vector3(textureSize*2, 0, textureSize*2)));
                currX += textureSize;
                if (currX == to)
                {
                    currX = from;
                    currZ += textureSize;
                }

            }




            
        }

        private void createBuildings()
        {
            String wallTexture;
            float blocksX = buildingsX + buildingsX - 1;
            float blocksZ = buildingsZ + buildingsZ - 1;

            float buildingWidthX = WIDTH / blocksX;
            float buildingWidthZ = WIDTH / blocksZ;

            float currX = 0;
            float currZ = 0;

            float currHeight;

            Vector3 pos;
            Vector3 size;
            BuildingBlock building;
            

            for (int x = 0; x < buildingsX; x++)
            {
                for (int z = 0; z < buildingsZ; z++)
                {
                    currHeight = game.random.NextFloat(heightMin, heightMax);
                    wallTexture = selectWallTexture(currHeight);
                    pos.X = currX;//+ game.random.NextFloat(1, 10);
                    pos.Y = 0;
                    pos.Z = currZ;// + game.random.NextFloat(1, 10);
                    size.X = buildingWidthX +game.random.NextFloat(-1, 5);
                    size.Y = currHeight;
                    size.Z = buildingWidthZ + game.random.NextFloat(-1, 5);

                    new Building(game, pos, size, wallTexture, selectRoofTexture());
                    building = new BuildingBlock(pos, size);
                    buildings.Add(building);
               
                    currZ += 2 * buildingWidthZ;
                }
                currX += 2 * buildingWidthX;
                currZ = 0;
            }
           
        }
        private String selectRoofTexture()
        {
            String roofTexture;
        
            int i = game.random.Next(1, 5);

     

            roofTexture = "textures/roof" + i + ".png";


    
            return roofTexture;
        }
        private String selectWallTexture(float height) {
            String wallTexture;
            String select;
            int i = game.random.Next(1,5);

            if(height < shortTexture)
                select = "short";
            else if(height < midTexture) 
                select = "mid";
            else
                select = "tall";

            wallTexture = "textures/" + select + i + ".png";


            //System.Diagnostics.Debug.WriteLine(wallTexture);
            return wallTexture;
        }


     
    }

    class BuildingBlock
    {
        public Vector3 pos;
        public Vector3 size;
        public BuildingBlock( Vector3 newPos,Vector3 newSize)
        {
            pos = newPos;
            size = newSize;
        }
    }
}