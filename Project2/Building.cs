﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.Toolkit;
using SharpDX;
namespace Project2
{
    // Enemy class
    // Basically just shoots randomly, see EnemyController for enemy movement.
    using SharpDX.Toolkit.Graphics;
    class Building : GameObject
    {
        
        public Building(Project2Game game, Vector3 pos, Vector3 size, String wallTexture, String roofTexture)
        {
            game.Add(new Walls(game, pos, size, wallTexture));
            game.Add(new Roof(game, pos, size, roofTexture));
        }



    }

    class Walls : GameObject
    {
        Vector3 size;

        public Walls(Project2Game game, Vector3 pos, Vector3 size, String wallTexture)
        {
            this.game = game;
            this.pos = pos;
            this.size = size;
            type = GameObjectType.City;
            myModel = game.assets.CreateTexturedWalls(wallTexture, pos, size);
            world = Matrix.Translation(pos);
            this.effect = game.Content.Load<Effect>("BuildingShader");

            GetParamsFromModel();
        }

        public override void Draw(GameTime gametime)
        {
            GetParamsFromModel2();
            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(0, myModel.vertices, myModel.vertexStride);
            game.GraphicsDevice.SetVertexInputLayout(myModel.inputLayout);

            // Apply the basic effect technique and draw the object
            effect.Parameters["ModelTexture"].SetResource(myModel.Texture);
            effect.CurrentTechnique.Passes[0].Apply();

            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, myModel.vertices.ElementCount);
        }

        public override void Update(GameTime gameTime)
        {
            Matrix WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
            effect.Parameters["World"].SetValue(world);
            effect.Parameters["Projection"].SetValue(game.camera.Projection);
            effect.Parameters["View"].SetValue(game.camera.View);
            effect.Parameters["WorldInverseTranspose"].SetValue(WorldInverseTranspose);
        }
    }

    class Roof : GameObject
    {
        Vector3 size;

        public Roof(Project2Game game, Vector3 pos, Vector3 size,String roofTexture)
        {
            this.game = game;
            this.pos = pos;
            this.size = size;
            type = GameObjectType.City;
            myModel = game.assets.CreateTexturedSquare(roofTexture, pos, size);
            world = Matrix.Translation(pos);
            this.effect = game.Content.Load<Effect>("BuildingShader");  

            GetParamsFromModel();
        }

        public override void Draw(GameTime gametime)
        {
            GetParamsFromModel2();
            // Setup the vertices
            game.GraphicsDevice.SetVertexBuffer(0, myModel.vertices, myModel.vertexStride);
            game.GraphicsDevice.SetVertexInputLayout(myModel.inputLayout);

            // Apply the basic effect technique and draw the object
            effect.Parameters["ModelTexture"].SetResource(myModel.Texture);
            effect.CurrentTechnique.Passes[0].Apply();

            game.GraphicsDevice.Draw(PrimitiveType.TriangleList, myModel.vertices.ElementCount);
        }

        public override void Update(GameTime gameTime)
        {
            Matrix WorldInverseTranspose = Matrix.Transpose(Matrix.Invert(world));
            effect.Parameters["World"].SetValue(world);
            effect.Parameters["Projection"].SetValue(game.camera.Projection);
            effect.Parameters["View"].SetValue(game.camera.View);
            effect.Parameters["WorldInverseTranspose"].SetValue(WorldInverseTranspose);
        }

    }
}
