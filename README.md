Disclaimer:
We have created all the models and textures in this game. However we do not own the wonderful photos of the hoff.

## Compiling
This program should compile in visual studio the same as lab seven, with no extra dependencies.

## Playing
To play the game with a keyboard, use the arrow keys to move around, and space to jump. Collect as much money as you can in the designated time period.
If playing on a tablet, tilting foreward will move the player, and sideways to turn. To jump, swipe downwards on the screen and the y-axis change will be recognized as a ManipulationDelta event, the further your finger travels, the higher you jump. The ball cannot move backwards in tablet mode either, this makes it slightly more playable, as you can easily stop on the roof of buildings. (It does take a little while to get used to the tablet version).

## Objects
The objects such as the sphere and coins, were created ourselves from blender. The city was created in a similar fashion to project 1. It is dynamically generated on each run to create a city of the same size, but different buildings sizes and positions. The textures are selected depending on the height of the building, so only tall buildings have skyscraper textures. 

## Lighting 
We used a vertex shader for buildings, with a slight orange-red tint, to correspond with the skybox. For the sphere and the money, we used a pixed shader. All light sources correspond with the location of the sun in the skybox.

## Camera
The camera has been programmed to mimic the player, every time the player moves or turnes, the camera follows suit. The viewing angle also changes with respect to speed. However whenever the player stops suddenly, the angle adjusts smoothly to avoid a sudden change.
This viewing effect is not as visible in the tablet version, as speeding up is more difficult. 

## Screenshots
![ScreenShot](https://bitbucket.org/pilioussis/graphics-and-interaction-project-2/raw/4b8672d2e707c5da06b527a26b98c52c7a4a88ee/screenshots/1.png)
![ScreenShot](https://bitbucket.org/pilioussis/graphics-and-interaction-project-2/raw/4b8672d2e707c5da06b527a26b98c52c7a4a88ee/screenshots/2.png)
![ScreenShot](https://bitbucket.org/pilioussis/graphics-and-interaction-project-2/raw/4b8672d2e707c5da06b527a26b98c52c7a4a88ee/screenshots/3.png)
